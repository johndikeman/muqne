<!doctype html>
<html>
    <head>
        <?php
        // the shared head  
        readFile("./components/head.html");
        ?>

        <link rel="stylesheet" href="./bin/css/home.css">
    </head>
    <body>
<?php
        include "./components/nav.php";
?>
        <div class="container">
            <div class="row">
                <div class="col-sm"></div>
                <div class="col-6">
                    <div class="row">
                        <p class="float-right text-muted font-italic">&lt;pronounced moo-shnay&gt;<p>
                        <p>muqne is an online toolkit for creating and playing a roleplaying game.</p>
                    </div>
                    <div class="row">
                        <h1 class="left-unindent">design philosophy</h1><br>
                    </div>
                    <div class="row">
                        <p>roleplaying games are fundamentally about freedom. muqne is a framework at its core- it provides a ruleset, handy tools for players and their dungeon masters, a rich and varied world to explore, and of course the technical capability to run a campaign no matter where any single participant is located. however, muqne doesn't lose sight of its roots- it's very much a human controlled experience, and gets out of the way as much as possible to allow all the
                        creativity and fun that makes traditional RPGs a timeless experience.<p> 
                    </div>
                    <div class=row>
                        <h1 class="left-unindent">features</h1>
                    </div>
                    <div class=row>
                        <ul>
                            <li>a handcrafted ruleset, taking inspiration from the best of many RPGs and integrating many unique and fun features</li>
                            <li>map and token system- easily create and mantain many maps at once and allow players to control their movemement</li>
                            <li>create multiple character sheets and play in multiple games across your persistent account</li>
                            <li>written history of the world of muqne- plenty of inspiration and room for crafting your collaborative story</li>
                            <li>pre-written classes, races, abilities, and spells with customization options</li>
                            <li>many more!</li>
                    </div>
                    </ul>
                        
                </div>
                <div class="col-sm"></div>
            </div>
            

        </div>
        <footer class="footer mt-auto py-3">
            <div class="container">
                john dikeman, november 4 2019
            </div>
        </footer>
    </body>
</html>




    
