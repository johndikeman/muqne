<?php

include "./../../env.php";

function login($username, $password){
    session_start();
    # Create connection
    $conn = new mysqli(HOST, USERNAME, PASSWORD, DB);
    $username = $conn->real_escape_string($username);
    $password = $conn->real_escape_string($password);

    $statement = "select * from Users where username='$username' and password='$password';";
    $res = $conn->query($statement);
    if(!$res){
        echo "query failed";

    }else{
        $acc = $res->fetch_assoc();

        if($acc){
            $_SESSION["account"] = $acc["id"];
            echo "yes";
        }else{
            echo "username and password not recognized!";
        }
    }
    $conn->close();

}

function create_account($username, $password){
    # Create connection
    $conn = new mysqli(HOST, USERNAME, PASSWORD, DB);
    $username = $conn->real_escape_string($username);
    $password = $conn->real_escape_string($password);

    # $crypt_pw = crypt($password,"insecuresalt");
    file_put_contents("./log","$username, $password\n");

    $statement = "insert into Users (username, password) values ('$username', '$password');";
    $res = $conn->query($statement);

    if($res){
        echo "success!";
    }else{
        echo "there was a database error";
    }
    $conn->close();

}

function check_if_taken($username){
    $conn = new mysqli(HOST, USERNAME, PASSWORD, DB);
    $username = $conn->real_escape_string($username);

    $statement = "select * from Users where username='$username';";
    $res = $conn->query($statement);

    if($res->num_rows > 0){
        echo "no";
    }
    $conn->close();
}


if(isset($_POST["username"])){
    login($_POST["username"],$_POST["password"]);
}else if(isset($_POST["potential_username"]) && isset($_POST["potential_password"])){
    create_account($_POST["potential_username"],$_POST["potential_password"]);
}else if(isset($_POST["considering_username"])){
    check_if_taken($_POST["considering_username"]);
}

?>
