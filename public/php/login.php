<html>
    <head>
        <?php
            readFile("../components/head.html");
        ?>
        <script src="../bin/js/login.js"></script>
        <style>
            input#password{
                margin-bottom:30px;
}
            #create-account-password-2{
                margin-bottom:30px;
            }
</style>
    </head>
    <body>
        <?php
            require "../components/nav.php";
        ?>
<div class="container">
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <div class="login-page">
                <h2>login</h2>
                <label for="username">username</label>
                <input id="username" class=form-control type="text">
                <label for="password">password</label>
                <input id="password" class=form-control type="password">
                <button class="btn-primary" id="submit-button">submit</button>
                <a id=show-create-account href="#">don't have an account? sign up!</a>

            </div>
            <div class="signup-page">
                <h2>sign up</h2>
                <label for="create-account-username">username</label>
                <input id="create-account-username" class=form-control type="">
                <label for="create-account-password-1">password</label>
                <input id="create-account-password-1" class=form-control type="password">
                <label for="create-account-password-2">repeat password</label>
                <input id="create-account-password-2" class=form-control type="password">
                <button id=create-account-submit class="btn-primary">submit</button>
            </div>
        </div>
        <div class="col-sm"></div>
    </div>
</div>
