<?php
session_start();

include "./../../env.php";

function add_character_to_db($character_json_string,$source=NULL){
    $character_assoc_array = json_decode($character_json_string,true);
    # Create connection
    $conn = new mysqli(HOST, USERNAME, PASSWORD, DB);
    $owner = NULL; # this should be the id of the currently logged in account
    if($source){
        $owner = $source;
    }else{
        $owner = $_SESSION["account"];
    }
    $firstname = $conn->real_escape_string($character_assoc_array["firstname"]);
    $lastname = $conn->real_escape_string($character_assoc_array["lastname"]);
    $bio = $conn->real_escape_string($character_assoc_array["bio"]);
    $culture_group = $conn->real_escape_string($character_assoc_array["culture_group"]);
    $class = $conn->real_escape_string($character_assoc_array["class"]);
    $race = $conn->real_escape_string($character_assoc_array["race"]);
    # we want these to go ahead and be string reprentations of json since that's what mysql will want
    $abilities = json_encode($character_assoc_array["abilities"]);
    $attributes = json_encode($character_assoc_array['attributes']);

    $statement = "insert into Characters (owner, firstname, lastname, bio, culture_group, class, race, abilities, attributes) values ($owner,'$firstname','$lastname','$bio','$culture_group','$class', '$race', '$abilities','$attributes');";


    $res = $conn->query($statement);
    $conn->close();

}

function get_characters_from_db($source = NULL){
    $conn = new mysqli(HOST, USERNAME, PASSWORD, DB);
    $owner = NULL; # this should be the id of the currently logged in account
    if($source){
        $owner = $source;
    }else{
        $owner = $_SESSION["account"];
    }

    $statement = "select * from Characters where owner='$owner';";
    $res = $conn->query($statement);
    $arr = $res->fetch_all(MYSQLI_ASSOC);
    return json_encode($arr);


}
if(isset($_POST["character"])){
    add_character_to_db($_POST["character"]);
}elseif(isset($_POST["get"])){
    echo get_characters_from_db();
}



?>
