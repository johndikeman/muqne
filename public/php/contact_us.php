
<html>
    <head>
        <?php
            readFile("../components/head.html");
        ?>
        <script src="../bin/js/character_creator.js"></script>
        <link rel="stylesheet" href="../bin/css/character_creator.css">
    </head>
    <body>
        <?php
            require "../components/nav.php";
        ?>

        <div class="container">
        <div class="row">
            <div class="col">
            </div>
            <div class="col-8">
                <h1 class="left-unindent">john dikeman</h1>
                <p>has been writing RPGs since junior high, where he ran a very ad-hoc lunch table campaign out of a ratty spiral for his friends. disheartened by the lack of free ways to play a traditional RPG with friends that might not all be able to come to the same table, he set out to write his own.</p> 
                <p class="text-muted">jrobdikeman@gmail.com</p>
            </div>
            <div class="col"></div>
        </div>
        </div>
</body>
</html>
