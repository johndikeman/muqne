<!DOCTYPE html>
<html lang="en">
<head>
    <?php
        readFile("../components/head.html");
    ?>
    <script src="../bin/js/lore.js"></script>
    <style>
      td{
        border:1px solid black;
        padding:5px;
      }
      img{
        width: 50%;
      }
    </style>
</head>
<body>
    <?php
        require "../components/nav.php";
    ?>
    <div class="container">
        <div class="row">
            <div class="col-4">
                <ul id="topics-list" class="list-group"></ul>
            </div>
            <div id="topic-info" class="col-8">
            </div>
        </div>
    </div>

</body>
</html>
