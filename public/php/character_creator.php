<html>
    <head>
        <?php
            readFile("../components/head.html");
        ?>
        <script src="../bin/js/character_creator.js"></script>
        <link rel="stylesheet" href="../bin/css/character_creator.css">
    </head>
    <body>
        <?php
            require "../components/nav.php";
        ?>
        <div id="maincontainer" class="container">
            <div class="row">
                <div id="left-sidebar-container" class="col-2">
                    <div class="list-group built-character-list">
                        <a class="list-group-item list-group-item-action new_character" href="">new character</a>
                    </div>
                </div>
                <div id="character-content-container" class="col-7">
                    <form action="">
                        <h1>new character</h1>
                        <div class="form-row">
                            <div class="col"><input class=form-control id="char-firstname" type="text" name="char-firstname" placeholder="first name"></div>
                            <div class="col"><input class=form-control id="char-lastname" type="text" name="char-lastname" placeholder="last name"></div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <label for="race">race</label>
                                <select id="race" class="custom-select" name="race">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="culture-group">culture group</label>
                                <select id="culture-group" class="custom-select" name="culture-group">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="class">class</label>
                                <select id="class" class="custom-select" name="class"></select>
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <input class=form-control id="bio" type="text" name="bio" placeholder="your character's biography">
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-3">
                                    <ul id="ability-list" class="list-group">
                                    </ul>
                                </div>
                                <div class="col-8">
                                    <h3 id="ability-name"></h1>
                                    <div id=ability-text></div>
                                </div>
                            </div>
                        </div>
                    </form>
                   <button id="submit-button" class="btn btn-primary" type="button">submit</button>
                </div>
                <div id="character-bio" class="col-3">
                    <div id="character-name"></div>
                    <div id="character-bio-snippet"></div>
                    <div id="race-desc"></div>
                    <div id="culture-group-desc"></div>
                    <div id="class-desc"></div>
                </div>
            </div>
        </div>

    </body>

</html>
