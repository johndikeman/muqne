<?php
session_start();
if(trim($_SERVER["DOCUMENT_ROOT"]) == "/application/public"){
    define("ROOT","");
}else{
    define("ROOT", "/cs329e-mitra/jrob/muqne/public");
}
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Muqne</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href=<?=ROOT ."/index.php"?>>Home<span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href=<?=ROOT ."/php/lore.php"?>>Lore</a>
<?php
if(isset($_SESSION["account"])){
?>
      <a class="nav-item nav-link" href=<?=ROOT ."/php/character_creator.php"?>>Character Creator</a>
      <a class="nav-item nav-link" href=<?=ROOT ."/php/under_construction.php"?>>Play!</a>
      <a class="nav-item nav-link" href=<?=ROOT ."/php/logout.php"?>>Logout</a>
<?php
}
?>
    <a class="nav-item nav-link" href=<?=ROOT ."/php/contact_us.php" ?>>Contact Us</a>
<?php
if(!isset($_SESSION["account"])){
?>
    <a class="nav-item nav-link" href=<?=ROOT . "/php/login.php"?>>Login</a>

<?php
}
?>
    </div>
  </div>
</nav>
