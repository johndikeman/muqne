



races = {
    "human":{
        "size":"medium",
        "bonuses":{},
        "description":"a peculiar folk. ambitious and foolish."

    },
    "elf":{
        "size":"medium",
        "bonuses":{
            "knowledge_nature":4
        },
        "description":"elves traditionally live communally in their sprawling forest homeland."
    },
    "dwarf":{
        "size":"small",
        "bonuses":{
            "knowledge_tech":4
        },
        "description":"dwarves are natural tinkerers and prefer to spend time below the ground."
    },
    "orc":{
        "size":"large",
        "bonuses":{
            "intimidate":4
        },
        "description":"monstrous and bred for war."
    }
}

culture_groups = {
    "hermit":{
        "name":"hermit",
        "race_req":[],
        "description":"you've chosen to isolate yourself from society.",
        "bonuses":{},
    },
    "divine_worshipper":{
        "name":"divine_worshipper",
        "race_req":["human","orc"],
        "description":"a dedicant of one of the many gods of muqne.",
        "bonuses":{
            "knowledge_divinity":4
        }
    },
    "gi'ian":{
        "name":"gi'ian",
        "race_req":["human","orc"],
        "description":"a member of one of the many manors of muqne",
        "bonuses":{}
    },
    "elf_regular":{
        "name":"elf_regular",
        "race_req":["elf"],
        "description":"a full member of the elven civilisation",
        "bonuses":{
            "diplomacy":2,
        }
    },
    "elf_banished":{
        "name":"elf_banished",
        "race_req":["elf"],
        "description":"you were banished from elf civilisation for one reason or another.",
        "bonuses":{
        }
    },
    "elven_dedicant":{
        "name":"elven_dedicant",
        "race_req":[],
        "description":"you were recruited by traveling elven missionaries to join the elven civilisation. you usually perform manual labor and other not fun tasks.",
        "bonuses":{

        }
    },
    "dwarf":{
        "name":"dwarf",
        "race_req":["dwarf"],
        "description":"a member of the dwarven civilisation, living in some underground colony.",
        "bonuses":{

        }
    }
}

classes = {
    "priest":{
        "name":"priest",
        "culture_group_req":["divine_worshipper"],
        "description":"you're a priest devoted to your god, and have access to more powerful prayers and rituals."
    },
    "warrior":{
        "name":"warrior",
        "culture_group_req":[],
        "description":"you're a battle-hardened warrior, trained in your choice of weapon and style."
    },
    "barbarian":{
        "name":"barbarian",
        "culture_group_req":[],
        "description":"you're a wild one- the slay first, ask questions later type."
    },
    "bard":{
        "name":"bard",
        "culture_group_req":["elven_dedicant","elf_regular","elf_banished"],
        "description":"you manipulate magick with music from specially designed instruments."
    },
    "rogue":{
        "name":"rogue",
        "culture_group_req":[],
        "description":"you prefer to move in the shadows."
    },
    "ranger":{
        "name":"ranger",
        "culture_group_req":[],
        "description":"you're an expert in tactics, and rather dispatch targets from a long range."
    },
    "warlock":{
        "name":"warlock",
        "culture_group_req":[],
        "description":"You draw your power from a lesser demon, bound to a particular item that you have."
    }
}
/* the schema for these could be
 *{name, type, description, culture_group_req,class_req, race_req}
*/
abilities = [
  {
    "name": "cleave",
    "class_req": "warrior",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "bonus",
    "description": "Cleave can be used to target multiple enemies with an attack. After the attack, youre staggered for an amount of turns equal to the number of enemies you hi"
  },
  {
    "name": "parry",
    "class_req": "warrior",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "immediate",
    "description": "You can choose to parry an oncoming attack, if you win a dex check with the DC being the attack roll of the opponent. If you get it, you take no damage and the opponent is staggered for 3 turns. If you miss, you leave yourself exposed & the enemy gets advantage on their damage roll."
  },
  {
    "name": "agressive stance",
    "class_req": "warrior",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Activating this ability gives you a permanent +2 on attack rolls but -1 on AC. each point in this ability increases the attack roll bonus by 1 and increases the AC penalty by ½."
  },
  {
    "name": "defensive stance",
    "class_req": "warrior",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Activating this ability gives you a permanent +2 on AC but -1 on attack rolls. each point in this ability increases the AC bonus by 1 and increases the attack roll penalty by ½."
  },
  {
    "name": "rage",
    "class_req": "barbarian",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "bonus",
    "description": "Receive advantage on all strength checks. Take a -2 to AC. gain a bonus equal to the level of the rage ability on all melee attack rolls. The rage can be stopped at any time. to continue it, make a strength check with a DC of 15 + the number of rounds that youve been raged. The ability can be used again after you make the finishing blow on an enemy."
  },
  {
    "name": "reckless attack",
    "class_req": "barbarian",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Get advantage on your first melee attack and damage rolls, enemies get the bonus on you on their next turn."
  },
  {
    "name": "vanish",
    "class_req": "rogue",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Use a smoke bomb to instantly succeed an initial stealth check. People are (obviously) aware that youre stealthing when you use this method, and may try to search for you. Each level gives a +1 bonus to stealth checks."
  },
  {
    "name": "blink",
    "class_req": "rogue",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "movement",
    "description": "You move so quickly that youre undetectable to most. Move up to half your regular movement speed instantaneously, without tripping traps or having to make any stealth checks."
  },
  {
    "name": "backstab",
    "class_req": "rogue",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "When in stealth, make a melee attack with advantage on both the attack rolls and damage rolls."
  },
  {
    "name": "ventriloquist",
    "class_req": "rogue",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "bonus",
    "description": "While in stealth, you can imitate any persons voice coming from a different direction. Make a bluff check with a DC of 15+the opponents wisdom mod."
  },
  {
    "name": "easy riff",
    "class_req": "bard",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Make a performance check against a DC of 15. If you succeed, gain pp equal to your current momentum divided by two. If you fail, your current momentum is divided in half."
  },
  {
    "name": "moderate riff",
    "class_req": "bard",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "20 performance points are required, but not consumed. Make a performance with a DC of 20. If you succeed, gain pp equal to your current momentum. You momentum is increased by 5. If you fail, lose 10 pp and momentum."
  },
  {
    "name": "difficult riff",
    "class_req": "bard",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "30 performance points are required, but not consumed. Make a performance check with a DC of 25. If you succeed, gain pp equal to your momentum and your momentum is increased by ten. If you fail, lose 20"
  },
  {
    "name": "motivating riff",
    "class_req": "bard",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "spend 100pp to give one of your allies one extra of any kind of action on their turn."
  },
  {
    "name": "disturbing riff",
    "class_req": "bard",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "spend 20pp, all enemies are shaken."
  },
  {
    "name": "inspiring riff",
    "class_req": "bard",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "spend 20 pp, all allies take a +2 bonus on attack rolls and AC for one turn."
  },
  {
    "name": "shrill riff",
    "class_req": "bard",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "spend 30pp, one enemy is frightened for one turn"
  },
  {
    "name": "focus",
    "class_req": "ranger",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "While prone, focus on a target. Gain +2 on your attack modifier and add 1d4 to your damage roll. This is only for the one target, if you switch targets your bonuses will be reset. Focus is also tracked seperately, and is used to cast other marksman abilities"
  },
  {
    "name": "countersnipe",
    "class_req": "ranger",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "gain 2 focus on an enemy that's using a ranged attack"
  },
  {
    "name": "spot",
    "class_req": "ranger",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Spend 2 focus, give an ally +2 to AC for one turn."
  },
  {
    "name": "disarm",
    "class_req": "ranger",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Spend six focus and shoot a weapon out of an opponents hand"
  },
  {
    "name": "subjugate",
    "class_req": "warlock",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Make a wis check against your demon. If you win, heal yourself and damage the demon for 1d4. If you fail, do the opposite."
  },
  {
    "name": "channel",
    "class_req": "warlock",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "You allow a portion of the demon into this reality, letting its fury loose indiscriminately. Deal 1d4 + the amount of health missing from the demon of damage to yourself and all enemies. Add the d4 roll to the demons health."
  },
  {
    "name": "possess",
    "class_req": "warlock",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Release your demon and allow it to manipulate a particular creature- tricking it into seeing things that arent there, whispering lies. Creature must be sentient. Every turn, your demon makes a will check against the creature. If it succeeds, the demon will manipulate it into doing something. If it fails or the creature dies, it will return to the item."
  },
  {
    "name": "prayer to xi seli, for healing",
    "class_req": "",
    "culture_group_req": "divine_worshipper",
    "race_req": "",
    "requirements": "",
    "action_type": "",
    "description": "Spend an hour in dedication to Xi Seli and she will award you 1d8 hit points"
  },
  {
    "name": "ritual to xi seli for healing",
    "class_req": "priest",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Spend five uninterrupted turns (thirty seconds) doing a ritual to Xi Seli, then heal a person of your choice for 1d6."
  },
  {
    "name": "prayer to gvju, for confidence",
    "class_req": "",
    "culture_group_req": "divine_worshipper",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Give an ally an advantage on whatever their next roll is. Upgrading this ability allows the bonus to be given to multiple allies"
  },
  {
    "name": "prayer to gvju, for vision",
    "class_req": "",
    "culture_group_req": "divine_worshipper",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Give an ally +3 to a perception check. Upgrading this gives +1 to the bonus"
  },
  {
    "name": "ritual to gvju, for cleansing light",
    "class_req": "priest",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Spend eight uninterupted turns (48 seconds) doing the ritual, then blind all enemies for three turns."
  },
  {
    "name": "prayer to tiju, for demonic protection",
    "class_req": "",
    "culture_group_req": "divine_worshipper",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Give a demonic enemy -3 on attack rolls for one turn. Upgrading this prayer adds one to the debuff."
  },
  {
    "name": "ritual to tiju, for demonic assistance",
    "class_req": "priest",
    "culture_group_req": "",
    "race_req": "",
    "requirements": "",
    "action_type": "standard",
    "description": "Spend four uninterrupted turns, then confuse an enemy for three turns."
  }
]

module.exports = {
    "races":races,
    "culture_groups":culture_groups,
    "abilities":abilities,
    "classes":classes,
}
