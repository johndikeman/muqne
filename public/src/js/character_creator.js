// this is a function that adds the built character objects retrieved from teh object_store endpoint to the sidebar
window.populate_list = function(){
    character_list = $.ajax(
        "../php/character_object_store.php",
        {
            data:{"get":"yes"},
            method:"POST",
            success:function(data){
                console.log("success in the ajax function")
                // assemble a list of the json'd characters
                window.character_object_list = JSON.parse(data);
                console.log(window.character_object_list);


                //data.forEach(function(val,ind){
                //    window.character_object_list.push(JSON.parse(val));
                //})

                // remove all the ones in there currently so there's not a billion every tiem
                $(".character-list-element").remove();

                window.character_object_list.forEach(function(character,ind){
                    console.log(character);
                    // character = JSON.parse(character);
                    var element = $(`<a class="list-group-item list-group-item-action character-list-element" character_ind=${ind}>${character["firstname"]}</a>`);

                    element.click(function(event){
                        var character_object = window.character_object_list[parseInt($(event.target).attr("character_ind"))];
                        console.log("in the list element click");
                        console.log(character_object);

                        $("#char-firstname").val(character_object.firstname).change();
                        $("#char-lastname").val(character_object.lastname).change();
                        $("#race").val(character_object.race).change();
                        $("#culture-group").val(character_object.culture_group).change();
                        $("#class").val(character_object.class).change();
                        $("#bio").val(character_object.bio).change();
                        $("#submit-button").attr("disabled","true");
                    });

                    $(".built-character-list").append(element);
                })
            }
        }
    )

}
window.onload = function(){
    _ = require("underscore");
    char_creation_stuff = require("./../../utils/char_creation.js");
    window.populate_list();

    $("#submit-button").click(function(event){
        console.log("submit clicked");
        var serialized = JSON.stringify(window.character);
        $.ajax(
            "../php/character_object_store.php",
            {
                data:{"character":serialized},
                method:"POST",
                success:function(){
                    console.log("first ajax success");
                    window.populate_list();
                }
            },
        );
    })
    // global variable for the character in progress
    window.character = {};
    $("#submit-button").attr("disabled","true");
    race_select = $("#race");
    race_select.empty();
    $("#race").append($("<option value='' selected>---</option>"))
    // iterate over the races and add them to the race selection
    $.each(char_creation_stuff.races,function(key,val){
        // constructing the option element that has the race name as the value and the actual text
        var new_elem = $(`<option value=${key}>${key}</option>`);
        // add that element to the list
        $("#race").append(new_elem);

    })
    $("#char-firstname").on("change",function(event){
        window.character.firstname = $(event.target).val();
        $("#character-name").html(`${window.character.firstname} ${window.character.lastname}`);
    })
    $("#char-lastname").on("change",function(event){
        window.character.lastname = $(event.target).val();
        $("#character-name").html(`${window.character.firstname} ${window.character.lastname}`);
    })

    $("#bio").on("change",function(event){
        window.character.bio = $(event.target).val();
        $("#character-bio-snippet").html(window.character.bio);
    })
    race_select.on("change", function(event){
        window.character.race = $(event.target).val();
        // set the right sidebar race description to the value there
        $("#race-desc").html(`you're a ${window.character.race}, ${char_creation_stuff.races[window.character.race].description}`);
        $("#culture-group").empty();
        // a culture group is considered valid for this race if it has the race in its requirements or if its requirements are empty (ie unrestricted)
        var valid_culture_groups = _.filter(char_creation_stuff.culture_groups,function(group){
            return group.race_req.length == 0 || group.race_req.includes(window.character.race);
        })
        console.log(valid_culture_groups);
        $("#culture-group").append($("<option value='' selected>---</option>"))
        $.each(valid_culture_groups,function(culture_group_name,culture_group){
            $("#culture-group").append($(`<option value=${culture_group.name}>${culture_group.name}</option>`));
        })

    });
    $("#culture-group").on("change",function(event){
        window.character.culture_group = $(event.target).val();
        $("#culture-group-desc").html(`you're a ${window.character.culture_group}, ${char_creation_stuff.culture_groups[window.character.culture_group].description}`);
        var valid_classes = _.filter(char_creation_stuff.classes,function(class_obj){
            console.log(class_obj);
            return (class_obj.culture_group_req.length == 0 || class_obj.culture_group_req.includes(window.character.culture_group));
        })
        var class_list = $("#class");
        class_list.empty();
        console.log(valid_classes);
        $("#class").append($("<option value='' selected>---</option>"))
        $.each(valid_classes,function(ind,class_obj){
            $("#class").append($(`<option value=${class_obj.name}>${class_obj.name}</option>`));
        })
    })

    $("#class").on("change",function(event){
        window.character["class"] = $(event.target).val();
        $("#class-desc").html(`you're a ${window.character.class}, ${char_creation_stuff.classes[window.character["class"]].description}`)
        var ability_list = $("#ability-list");
        var ability_name_field = $("ability-name");
        var ability_desc_field = $("ability-text");
        ability_list.empty();
        ability_desc_field.empty();
        ability_name_field.empty();

        var valid_abilities = _.filter(char_creation_stuff.abilities,function(ability){
            return (!ability.culture_group_req || ability.culture_group_req==window.character.culture_group) && (!ability.race_req || ability.race_req == window.character.race) && (!ability.class_req || ability.class_req == window.character["class"]);
        })

        var ability_list_to_add_to_character = [];
        $.each(valid_abilities,function(ind,ability_obj){
            // make the ability objects by changing them into a object with a level
            ability_list_to_add_to_character.push({name:ability_obj.name,desc:ability_obj.description,level:0});
            // create the element for the ability in the left panel bit
            var ability_list_element = $(`<li class="list-group-item ability_option">${ability_obj.name}</li>`);
            // make the click handler for that element
            ability_list_element.on("click",function(event){
                //remove all the active classes from every list element first
                siblings = $(event.target).parent().children();
                $.each(siblings,function(ind,element){
                    $(element).removeClass("active");
                })
                // then add the active class on the one that was clicked
                $(event.target).addClass("active");
                // getting the ability object associated with the ability that was clicked
                ability_obj = _.find(char_creation_stuff.abilities,function(elem){
                    return elem.name == $(event.target).html();
                },this);
                $("#ability-name").html(ability_obj.name);
                $("#ability-text").html(ability_obj.description);
            })
            $("#ability-list").append(ability_list_element);
            //when we're on the last iteration of the loop and the list that we want on the character object has been fully populated
            if(ind === valid_abilities.length - 1){
                // this will make a clone of that array. just trying to ward off any weird reference issues
                window.character["abilities"] = ability_list_to_add_to_character.slice(0);
                window.character["attributes"] = [
                    {
                        name:"strength",
                        value:0,
                        max:-1,
                    },
                    {
                        name:"hitpoints",
                        value:10,
                        max:10,
                    },
                    {
                        name:"dexterity",
                        value:0,
                        max:-1,
                    },
                    {
                        name:"constitution",
                        value:0,
                        max:-1,
                    },
                    {
                        name:"intelligence",
                        value:0,
                        max:-1,
                    },
                    {
                        name:"wisdom",
                        value:0,
                        max:-1,
                    },
                    {
                        name:"charisma",
                        value:0,
                        max:-1,
                    },
                ];

                $("#submit-button").removeAttr("disabled");
            }
        })

    });
}
