window.onload = function(){
    var populate_lore = async function(){
        lore = require("./lore_raw");
        return await lore;
    };
    populate_lore().then(function(lore){
       Object.entries(lore).map(function(tuple,ind,array){
            var key = tuple[0];
            console.log(key);
            var val = tuple[1];
            console.log(val);
            console.log("here");
            var new_element = $(`<li class=list-group-item val="${key}">${key}</li>`);
            new_element.on("click",function(event){
                console.log(lore[$(event.target).attr("val")]);
                $("#topic-info").html(lore[$(event.target).attr("val")]);
                //remove all the active classes from every list element first
                siblings = $(event.target).parent().children();
                $.each(siblings,function(ind,element){
                    $(element).removeClass("active");
                })
                // then add the active class on the one that was clicked
                $(event.target).addClass("active");
            })
            $("#topics-list").append(new_element);
           if(ind===0){
               new_element.click();
           }
        })
    })
}
