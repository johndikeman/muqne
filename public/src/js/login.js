bcrypt = require("bcryptjs");


window.onload = function(){
    $(".signup-page").hide();
    $("#show-create-account").click(function(event){
        $(".signup-page").show();
        $(".login-page").hide();
    });
    $("#submit-button").click(function(event){
        var username = $("#username").val();
        var pw = $("#password").val();
        console.log(username);
        console.log(pw);
        bcrypt.hash(pw,'$2a$10$d0SQ31xe8acnJLPPP771UO',function(err,hash){
            console.log(err);
            $.ajax("../php/accounts.php",{
                method:"POST",
                data:{
                    "username":username,
                    "password":hash,
                },
                success:function(ret){
                    ret = ret.trim();
                    if(ret === "yes"){
                        var success_alert = `<div class="alert alert-success" role="alert">successfully logged in!</div>`;
                        $(event.target).parent().prepend(success_alert);
                        window.location.replace("../index.php");
                    }else{
                        var fail_alert = `<div class="alert alert-danger">your username and password weren't recognized!</div>`;
                        $(event.target).parent().prepend(fail_alert);
                    }
                },
                error:function(request, status, error){
                    var fail_alert = `<div class="alert alert-danger">oops, something went wrong!</div>`;
                    $(event.target).parent().prepend(fail_alert);
                }
            })
        })
    });

    $("#create-account-username").focusout(function(event){
        $.ajax("../php/accounts.php",{
            method:"POST",
            data:{
                "considering_username":$(event.target).val()
            },
            success:function(ret){
                ret = ret.trim();
                console.log(`returned from check if taken: ${ret}`);
                if(ret=="no"){
                    var no_unique_warning = `<div class='alert alert-danger' role='alert'>that username is already taken!</div>`;
                    $(event.target).parent().prepend(no_unique_warning);
                    $(event.target).val("");
                }
            }
        })

    });
    $("#create-account-submit").click(function(event){
        console.log("here");
        var name = $("#create-account-username").val();
        var password = $("#create-account-password-1").val();
        var password2 = $("#create-account-password-2").val();
        console.log(password);
        console.log(password2);

        if(password !== password2){
            var fail_alert = `<div class="alert alert-danger">the passwords you entered do not match</div>`;
            $(event.target).parent().prepend(fail_alert);
        }else{
            bcrypt.hash(password,'$2a$10$d0SQ31xe8acnJLPPP771UO',function(err,hash){
                console.log(hash);
                $.ajax("../php/accounts.php",{
                    method:"POST",
                    data:{
                        "potential_username":name,
                        "potential_password":hash
                    },
                    success:function(ret){
                        var success_alert = `<div class="alert alert-success" role="alert">account created!</div>`;
                        $(event.target).parent().parent().prepend(success_alert);
                        console.log(ret);
                        $(".signup-page").hide();
                        $(".login-page").show();

                    }
                })
            });
        }
    });
}
