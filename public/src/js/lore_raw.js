module.exports = {
  "Languages of Muqne":`<h2>Languages of Muqne</h2>
  <h3>Gvsqi</h3>
  <p>Old High Gvsqi was the magical language of the gods. The gods didn&rsquo;t really use language, they just directly communicated concepts in a telepathy sort of way. During the Thousand Years War, the gods taught the first humans some words to give them a limited control of magic and make them better warriors.</p>
  <p> Vowels</p>
  <p><a></a><a></a></p>
  <table>
  <tbody>
  <tr>
  <td colspan="1" rowspan="1">
  <p>latin representation</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>International Phonetic Alphabet</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>i</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>i</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>e</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>ɜ</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>u</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>u</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>v</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>ə</p>
  </td>
  </tr>
  </tbody>
  </table>
  <p>(gvsqi is also a tonal language, each vowel has three tones- high, rising, and low)</p>
  <p>Consonants</p>
  <p><a></a><a></a></p>
  <table>
  <tbody>
  <tr>
  <td colspan="1" rowspan="1">
  <p>latin representation</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>International Phonetic Alphabet</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>m</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>m</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>n</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>n</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>b</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>ʙ (bilabial trill)</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>f</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>f</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>x</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>&theta;</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>t</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>&eth;</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>s</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>s</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>z</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>z</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>q</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>ʃ</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>g</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>&Chi; (uvular fricative)</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>j</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>j</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>c</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>| (dental click)</p>
  </td>
  </tr>
  </tbody>
  </table>
  <p>Old Low Gvsqi: keeping the phonemic structure and imitating the syntax of its High counterpart, this language that developed on the mainland was key to the Great Banishing, since it allowed humans and dwarves to communicate between one another with much greater sophistication. Was the result of a gradual broadening of vocabulary. The first written texts date back to approximately 700.</p>
  <p>After the Exodus, human language sorta splintered- the mainland magick users&rsquo; kept Old Low Gvsqi, which eventually evolved into what is generally known as New Low Gvsqi, or just Low Gvsqi. It shares some intelligibility with its Old counterpart, but it takes some skill to actually read the ancient texts. </p>
  <p>The Mainland Divine Worshippers that stayed on land carried out an organised restructuring of Old Low Gvsqi to remove aspects they deemed blasphemous, this included altering syntax and replacing many semantics. This language is generally known as Northern Divine. This language basically predates the tribal wars, after which you see some variation between chiefdoms (but all the variations maintain a pretty high level of mutual intelligibility)</p>
  <h3>Dwarven</h3>
  <p>The Dwarves developed their language from Old Low Gvsqi, it&rsquo;s not mutually intelligible with any of its linguistic relatives. It&rsquo;s called (surprise) dwarven.</p>
  <h3>Elven</h3>
  <p>Elven is a language isolate, since the elves were never actually taught any Old High Gvsqi. Its phonology is highly dependent on the vocal and sensory physiology of the elven race, which includes a very powerful diaphragm and two sets of mandibles- inner and outer. Contrastive sounds in Elven are generally composed of combinations of microtonal variations in a whistled pitch and clicks/chirps, articulated with one or both sets of mandibles. Elven also has a marked register articulated solely through whistling, which allows for long distance communication, as each individual can produce a very loud tone.</p>
  <p>As Elven is highly dependent on the unique form of the elves, it&rsquo;s basically unarticulatable to other species. A signed language was innovated through trade that allows interspecies communication.</p>
  <h3>Infernal</h3>
  <p>All Infernals were taught a language by Tiju, called Infernal. All demons continue to speak infernal, but orcs don&rsquo;t really talk.</p>
  <h3>Common</h3>
  <p>Humans on the island developed a language (also descended from Old High Gvsqi) that&rsquo;s generally called Old Island Gvsqi. After the great banishing, orcs and humans and elves and dwarves alike were enslaved en masse by the most powerful warlords, and eventually cultural exchange and interbreeding brought about Common, which resulted from a blending of Infernal and Old Island Gvsqi.</p>`,
  "The First Divinity":`<h2>The First Divinity</h2>
  <p>The first two divines came into being from nothing and the world was made with them- Gvsqi and Xi Seli.</p>
  <p>Gvsqi and Ix Chel had nine children, the first real divine generation</p>
  <p>Qum, the god of death</p>
  <p>Guxi and Gvji, twin gods of agriculture and the sun, respectively</p>
  <p>Tiju, God of infernals</p>
  <p>Biqu, Goddess of Animals and Monsters</p>
  <p>Xinu, God of War</p>
  <p>Tvqe, Goddess of Art and Creation</p>
  <p>Digv, God of Love</p>
  <p>Cuzu, Goddess of Deception</p>`,
  "The Thousand Years War":`<h2>The Thousand Years War</h2>
  <p>the God of Love and the Goddess of Deception have kids:</p>
  <p>The God of Lust</p>
  <p>The Goddess of Wrath</p>
  <p>The God of the Forest</p>
  <p>Gvsqi, having forbid his children from having children, attempts to kill all five of them. This sparks a war between the gods. The beginning of this war, later known as the thousand year war, marks the beginning of time.</p>
  <p>Gvsqi, Ix Chel, Tiju, the Goddess of Animals and Monsters, and Qum all fight to suppress Guxi and Gvji, the God of War, the Goddess of Art and Creation, the God of Love, the Goddess of Deception, the God of Lust, the Goddess of Wrath, and the God of the Forest.</p>
  <p>The gods fight for a hundred years or so, but it&rsquo;s pretty equal because they all get tired and have to recharge at about the same time. SO, Tiju has this idea- make warriors that can reproduce themselves at will, and make a bunch of them to overwhelm the other side. In the fall of 107 He made two distinct races- demons, capable of shifting in and out of reality and other ghostly stuff, and orcs, which were essentially magic powered supersoldiers that weren&rsquo;t that smart but were to be led by the demons, who were smart. The Goddess of Animals and Monsters also created (surprise) a bunch of animals and monsters as well as some elves, who promptly refused to fight and found a bunch of trees to hide from their new masters, with varying degrees of success.</p>
  <p>The other side fights for another two hundred years and is almost overwhelmed, but then in the spring of 314 the God of War creates a bunch of humans and the Goddess of Art and Creation creates a bunch of dwarves. To conserve magic since they were weak from fighting off the orc/demon/animal/monster horde they didn&rsquo;t imbue either race with magical essence, instead transferring them a LOT of creativity and ingenuity.</p>
  <p>So they go at it for another three hundred years or so, during all this time Old Low Gvsqi is being developed and the human legions are reproducing and forming camps and becoming better and better at cooperating with one another. Towards the end of this period, six elders with a lot of knowledge of Old High Gvsqi are becoming particularly popular as their wisdom is proven on the battlefield.</p>
  <p>During that same period of time, the gods keep making new gods, as they do. Qum horns in on the Goddess of Wrath, and she gives an incredibly agonizing birth to the Goddess of Pain and Blindness, (who is born blind). The spectacular magick energy released by this event is felt by everyone, and is noted by the six elders, who begin to formulate a plan. Guxi and the Goddess of Wrath get together and have three kids, the God of Chefs, and the twin Goddesses of Poison and Remedy. Tiju and the Goddess of Animals and Monsters bump uglies and make the God of Rulers and the Underworld, The Goddess of the Soil, the Goddess of the Air, the Goddess of the Ocean, and the Goddess of Refuse. Qum and the Goddess of Remedy produce the Goddess of Prophecy.</p>
  <p>While the gods are embroiled in their conflict, the humans are conspiring in their shiny new language the gods don&rsquo;t understand to overthrow them. The six elders harness powerful magic left as residue on a previous battlefield to lay a trap for the god of the forest. When the god, attracted by the torching of a forest, is caught in the trap, the humans swarm him and torture him by gouging out his flesh and puncturing his eyes with spears. The magickal energy released by his intense pain is stored in totems by the elders. When the god is weakened and starts putting out less energy, the elders constrict the bonds of the magic trap and he becomes the first and only divine casualty of the thousand years war. The catastrophic amount of energy produced by this event attracts the attention of the other gods, who are all fighting elsewhere. Using the energy in the totems and the god&rsquo;s essence, in the winter of 928 the elders banish all the gods in Muqne to the Ether. This event, called the Great Banishing, marks the end of the Divine Age.</p>`,
  "Pantheon":`<h2>Pantheon</h2>
    <a href="https://www.youtube.com/watch?v=CrRChyYhNl0">what does alignment mean?</a>
  <p><a></a><a></a></p>
  <table>
  <tbody>
  <tr>
  <td colspan="1" rowspan="1">
  <p>name</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>patronage</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>alignment</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>aspects</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>lore</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Gvsqi</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Writing, language, learning, sciences</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Neutral good</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>male</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>He&rsquo;s confident that he&rsquo;ll be able to keep his own children in check to keep Muqne a good place for all the gods to exist in peace, but he&rsquo;s afraid of the uncertainty more divines will bring.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Xi Seli</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Moon, weaving, medicine</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Neutral good</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>female</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>She is loyal to Gvsqi, but also feels compassion for the mother of the second generation.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Qum</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Death</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Chaotic Neutral</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Male, four large arms</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>First Generation. Sides with his father in the thousand years war because he thinks that he will win the war swiftly and brutally, and wants there to be death. He falls in love with Qinu, and accidentally fathers a child with her in secret. He does the same thing with Svgi later on. After that child is discovered, Gvsqi curses him by making him hold up muqne from the underworld, one arm on each corner.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Guxi</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Agriculture</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>neutral good</p>
  </td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>First Generation. Sides with his sister, as he views his father as cruel and power-hungry and feels an obligation to support new life.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Gvju</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Sun</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>True neutral</p>
  </td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>First Generation. Sides with his sister because he&rsquo;s afraid his immense power will start to threaten his father.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Tiju</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Infernals</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Lawful evil</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>jaguar</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>First Generation. Sides with Gvsqi in the Thousand Years War in the hopes that he will prove himself worthy to replace Xi Seli as Gvsqi&rsquo;s second in command.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Biqu</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Animals and Monsters</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>True Neutral</p>
  </td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>First Generation. Sides with her father cause her siblings used to make fun of her.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Xinu</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>War</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Chaotic Neutral</p>
  </td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>First Generation. Sides with his sister because he knows a war will happen and wants it to be fair and glorious.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Tvqe</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Art and Creation</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Neutral good</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>genderless</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Sides with their sister because they want to have children.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Digv</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Love</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Neutral Good</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>male</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>First Generation. Sides with his lover, because he loves her.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Cuzu</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Deception</p>
  </td>
  <td>Neutral</td>
  <td colspan="1" rowspan="1">
  <p>female</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Is the defacto leader of the other side. Wants her children to stay alive.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Segu</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Lust</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Second generation, born of Digv and Cuzu in the fall of 0.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Svgi</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Wrath</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Second generation, born of Digv and Cuzu in the fall of 0.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Meju</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Forest</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Second generation, born of Digv and Cuzu in the fall of 0.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Qiju</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Pain and Blindness</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Chaotic Evil</p>
  </td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Third Generation, born of Qum and Svgi in the spring of 900. Her eyes were gouged out by her mother after her birth, partly as punishment for the agony of her birth and partly as penance for being born from the enemy.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Futi</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Chefs</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Third generation, born of Svgi and Guxi in 613.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Zinu</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Poison</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Third generation, born of Svgi and Guxi in 613.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Qinu</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Remedy</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Third generation, born of Svgi and Guxi in 613.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Gime</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Rulers and the Underworld</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Second Generation, born of Tiju and Biqu in 745.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Cixu</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Soil</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Second Generation, born of Tiju and Biqu in 745.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Cune</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Air</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Second Generation, born of Tiju and Biqu in 745.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Cuxe</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Ocean</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Second Generation, born of Tiju and Biqu in 745.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Ciju</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Refuse</p>
  </td>
  <td></td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Second Generation, born of Tiju and Biqu in 745.</p>
  </td>
  </tr>
  <tr>
  <td colspan="1" rowspan="1">
  <p>Qisu</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>Prophecy</p>
  </td>
  <td colspan="1" rowspan="1">
  <p>True Neutral</p>
  </td>
  <td></td>
  <td colspan="1" rowspan="1">
  <p>Fourth generation, born of Qinu and Qum in 915.</p>
  </td>
  </tr>
  </tbody>
  </table>
`,
"Introduction to the Elders":`<h2>Introduction to the elders</h2>
<p>In the spring of 834, a child named Numbex Gi was born. After about his fifth birthday, as was custom in the militant human proto-society, he was removed from his mother and began a curriculum of military strategy and combat magick. He excelled in his studies, and soon began experimenting with Old High Gvsqi, discovering and documenting new spells that he meticulously recorded in a Compendium. Several of his classmates, envious of his skill, pushed themselves to attain his proficiency with magic. These five consisted of the girl Siqezu Tu, the boy Nquzenu Sqvcsu, the girl Xinecni Cvjinixu, the girl Zibu, and the boy Cijucxu. Eventually, the six formed a club of sorts, where they each tried to discover new magick and cooperatively add to the Compendium. Together, they found a lot of new magick- but their research had to be put on hold once they turned 18 and were stationed to the front lines of the mainland conflict. </p>
<p>Up until then, humans used magic in a pretty limited fashion in combat- mostly basic wards and energy missiles and healing. The six kids pioneered the technique of not using the weapons the gods provided to them, instead preferring to use both hands to cast spells, often simultaneously. Another invention that changed the game were Siquezu Tu&rsquo;s totems- objects that could be charged with more magick than what could be absorbed from the air by casters, allowing for arbitrarily powerful spells to be cast. For thirty years they destroyed scores of monsters with ruthless efficiency, and were eventually recognized by the leader of the human forces as exemplary in 872. He sought to arm all of his troops with the type of power that the six commanded, and thought that they would win the war if this were to happen. So, he ordered them to return to the main human encampment and take over the education of the young.</p>
<p>The six settled into their new duties, teaching by day and thinking and continuing their research by night. Among themselves, they began to consider what winning the thousand years war might entail. When the leader of the gods was defeated, they supposed that humans would find themselves under the jurisdiction of the gods that fought alongside them. They had seen scores of humans die for the gods, placed into situations where it was clear that they were not expected to win. They thought about what else could have been accomplished in the thirty years they spent slaughtering monsters. Eventually, they decided that the gods didn&rsquo;t much care about whether the human race lived or died after the war- they had only been created to defeat the other side. They found this existential uncertainty to be quite disturbing, and secretly began to work on a way to destroy the gods in 882.</p>
<p>Research on the source of aether, the incorruptible element that magick is composed of, proved fruitful in 890. A deep vault of aether was discovered, but it took an immense amount of magick to rip open a hole from the terrestrial world to the aether. In the presence of the other magi, Zibu became the first human to set foot into the aether, while the other five held open the portal. Her observations are recorded in the Compendium: all of the magick in her totem dissipated quickly among the aether, and she felt her sense of self beginning to melt away- she became at great peace, and was convinced that the aether was where she was supposed to be. When she stopped responding, she was recalled back into the terrestrial by the five. It took her three days to recover, and her personality was permanently altered. The amount of magick it would take to banish all the gods to the aether was calculated, and Siquezu Tu began construction of a set of six totems that could contain the power, which were completed in 895. It soon became apparent that charging the totems would take much too long, they needed a quicker source. The answer came in 900, when Qiju was born. The pain of her birth released incredible amounts of magick at a super fast rate. Then the idea to trap and torture a god was born, the rest is explained in the section on the thousand years war.</p>
`,
"Human post-divinity History":`<h2>Human post-divinity history</h2>
<h3>The Great Exodus</h3>
<p>The months after the Great Banishing were tough. The entire history of the human race was spent fighting a mostly meaningless war, and now the only constant in that sea of bloodshed and anguish had been sent to the ether. Some effects of this event weren&rsquo;t foreseen by the elders, and contributed to a backlash against them among the other humans. The magick energy constantly emanating from the gods kept most humans alive for at least four hundred years, accelerated the healing process, satisfied hunger and thirst, and gave strength. In the immediate aftermath of the banishing, people discovered what it felt like to hunger and thirst, and many saw their loved ones age hundreds of years in days and wither away before their very eyes. The elders hastily used their knowledge of magic to provide sustenance, but there was no way that they could provide enough. The humans split into two factions- ones that were loyal to the elders, and those who wanted the old gods back. The latter renounced magick, the brutal tool that exiled their gods, and spent their days trying desperately to connect with the gods in the ether through prayer and meditation. One day, a woman had a vision from the Goddess of Prophecy- she told her that the gods were all alive, just weak and limited in their power on Muqne, but that they would support them in a war of vengeance against the elders and their supporters. </p>
<p>The next day, aided by the goddess, the woman rallied support among the discontent in the elders&rsquo; young new society, and the Worshippers of Divinity were born. The elders and their people were caught off guard, but soon recovered and pretty much destroyed the Worshippers with their magic. The surviving members fled north, and were pursued for half a year- only resting to pray for help and forage off of the land. In the summer of 929, just as it seems all is lost, the Worshipper&rsquo;s Prayers were answered. Impossibly huge mountains sprung from the earth, killing thousands of the elders&rsquo; band, and one elder himself. This event would later be called the Exodus.</p>
<p>The Worshippers settled almost immediately in the frozen wasteland of the north. The remaining elders, finding no way to surmount the divine mountains, began a slow path back south with their nations in tow.</p>
<h3>Development of Mainland Civilisation</h3>
<p>After the great exodus and the tragic loss of Nquzenu in 929, the new roving warrior nation had nowhere to go, really. The pursuit had not only given them a purpose, it had gathered them around the single point of control that was Numbex Gi. They traveled back south, crossed over the river once more, and began construction on a capitol there. Using magick to extract the materials, the main castle was completed in about fifty years, in 979. During the age of divinity, the magick that emenated from the gods was plenty enough so that every person could perform magick on their own- some more skilled than others, but still it could be done. After the banishing, the magic didn&rsquo;t have the same flow, and could only be harnessed effectively by people who already owned/knew how to make totems. </p>
<p>Eventually, farmers noticed something peculiar- crops grew very poorly. Without Guxi around to make the plants grow quicker, it was not going to be possible for subsistence farming to work. Eventually it was figured out that they could get the crops to grow by inoculating the soil with magick. The elders worked out a deal with the peasants- in return for the means to grow their crops, they would surrender their crop to the regional authority and allow it to be taxed. Thus, post divinity mainland feudalism was born.</p>
<p>Eventually, Numbex divided up the entire mainland in between the other elders and one of the generals of the army. Numbex wanted to develop and bring as much of the land under his control as he could. He assigned each lord and lady 200 families, soldiers, enough supplies to make it to where they needed to go, and sent them off. They founded capitals in each of the manors, and then divided the other land between vassals who established towns and payed taxes to the lord. </p>
`,
"Mainland Human Classes":`<h2>Mainland Human Classes</h2>
<h3>Nobles</h3>
<p>Nobles are people that in some way are involved with the operation of a manor. These people included the lords and ladies that held sections of land underneath the five main lords, as well as those people&rsquo;s advisers and families. These people are guaranteed to have access to totems. In some cases, you will also see a section of academics that are supported by the lord or lady of a manor- these should also be considered nobles even though their function is quite different. </p>
<p>The rank of noble is entirely hereditary.</p>
<h3>Knights</h3>
<p>Knights are high-ranking soldiers loyal to the emperor. They&rsquo;re trusted commanders, who proved themselves to their lord and were promoted. They have good armor, adorned with the colors of their manor.</p>
<p>Their rank is not officially transmitted to their children, but it&rsquo;s easier for them to be promoted so it basically is.</p>
<h3>Enlisted</h3>
<p>Career soldiers who volunteered and receive a salary. This is important, since peasants and slaves can find themselves fighting under a banner but still retain their class identity and the lack of privileges that go with it. They received training somewhere and are generally issued armor bearing the standard of their army and a weapon, but that&rsquo;s not necessarily the rule. Their position is sorta analogous to that of an officer.</p>
<h3>Merchants</h3>
<p>These are people who ferry goods in between towns and sell them to make a profit. Most merchanting groups consist of small bands of mercenaries that are able to defend the shipment from bandits or monsters in exchange for a salary or a share in the profit. Some larger conglomerates exist, but these mostly take the form of cohesive tribes of divine worshippers who make their living completely off of trade.</p>
<h3>Peasants</h3>
<p>Common agricultural folk. They do subsistence farming, and pay taxes to their lord in the form of either money or a section of their harvest. Often the victims of exploitation from bandits, monsters, and the lord they toil under, the life of a peasant is hard. Its commonplace for a peasant to offer up their children for indentured servitude or permanent slavery in order to stay alive when the financial burden becomes too much to bear.</p>
<p>Not only are they the cornerstone of food and wealth production for the lord, they form the majority of the population of the various armies of mainland Muqne. When they are in this position they don&rsquo;t receive a salary as their service is being used to pay back whatever debt to the lord they owe. They are also on their own in terms of armor, which generally means they have none.</p>
<h3>Slaves</h3>
<p>While relatively uncommon, completely enslaved people form a separate class in mainland muqne. Unlike the indentured, whose contracts will eventually run out, the enslaved are owned by their masters until they die. Most of the slaves are owned by nobles, and do manual labor like construction or sanitation.</p>
<p>Enslaved folks are either captured enemies, divine worshippers captured by a predatory slave trading tribe, or peasants who were sold to make ends meet.</p>
`,
"Mainland Human Feudalism":`<h2>Mainland Human Feudalism</h2>
<img class="muqnemap" src="./../content/muqnemapcolors.jpg">
<h3>Numbex Gi&rsquo;s Manor</h3>
<p>The first territory of the new empire, marked in blue on the map, encompasses much of the central area of muqne. Surrounded on all sides by the manors of the other lords, this central location would ensure the security and economic wellbeing of the area. As a result of its location and its place in the hierarchy, most towns are wealthy and unwalled. </p>
<p>The first city in the territory is Gi&rsquo;ivn. A castle and a university of the magickal arts are both located here. This city also houses a large class of nobility, and a ton of knights. The largest training camp in the kingdom, recruiters prey on peasants, sometimes through convincing them to voluntarily enlist, or by conscripting them through indentured servitude if their harvests aren&rsquo;t adequate to pay their taxes.</p>
<p>The wealth of the nobility and knights also support a large merchant class, who are either traders from other manors or particular tribes of divine worshippers.</p>
<h3>Siqezu Tu&rsquo;s Manor</h3>
<p>Marked in green on the map, this area also encompasses the great forest that houses the elven civilisation. The main city, Xgine, is an urban center mainly composed of advanced magick practitioners seeking to further their understanding of the ancient art in one of the many institutions of higher learning in the city. The city is also the source of all the totems in Muqne, manufactured with Siqezu&rsquo;s close personal supervision. The city has high walls and there are many castles around the borders of the forest, due to suspicion of the mostly peaceful elven civilisation.</p>
<p>Officially, the worship of divinity is banned in this manor, so the merchant population (especially in the capitol) is mostly dwarven or from other manors, but individual cities may have lax enforcement of this rule for economic reasons.</p>
<p>Southern coastal regions support a large amount of crops and have higher populations as a result, but are often the victim of raids by bandits, aquatic divine worshipper tribes, and sea monsters.</p>
<h3>Xinecni Cvjinixu&rsquo;s Manor</h3>
<p>Marked in red on the map, this area is most of the western coast of Muqne. It&rsquo;s a relatively productive agricultural area, but it&rsquo;s hampered by the shorter growing seasons and the high concentrations of orc encampments. This manor mainly serves as a sort of buffer between the monsters and Gi&rsquo;ivn. There&rsquo;s not really a large urban center to be spoken of, the population lives in either forts or agricultural villages.</p>
<p>Due to the frequency of attacks, there is a high knight population and most of the peasants have combat experience.</p>
`,
"Reactions to the Great Banishing":`<h2>Reactions to the Great Banishing</h2>
<h3>Dwarven </h3>
<p>The Dwarves, who worked in mines and fashioned and piloted large war machines, were mostly apathetic about the disappearance of the gods- preferring instead to continue to build and progress. They followed the Exodus from a distance, and saw a huge opportunity for research, resources, and an ancestral home in the newly made mountains. They repurposed their war machines and used them to build labrinthine dwellings and extract valuable ores from the hearts of the mountains. They also discovered magma, and harnessed its power with steam in 948. They built an industrialised society that they kept secret within the mountain.</p>
<h3>Infernal </h3>
<p>After the Banishing, the Demons recognized the inferiority of the orcish forces against the human and dwarven hordes. Having the ability to phase in between reality and the ether, they recalled themselves there and tried to unravel the spells that trapped their divine leaders.</p>
<p>The mainland orcs, having little to no ability to think for themselves beyond a basic survival and reproductive instinct, were pretty much lost after the Banishing. They meandered around and got driven basically to extinction by the first millennia. The island orcs fared better- under the leadership of a band of humans, they created a nation.</p>
<h3>Elven</h3>
<p>The elves had been going since their creation about 900 years previously so they really didn&rsquo;t care, and by that point had a stable civilisation that valued order and peace, and used their magic to create huge complex tree fortresses.</p>
`,
"General Mechanics":`<h2>Mechanics</h2>
<h3>General mechanics</h3>
<h3>Saving throws</h3>
<p>Theres three kinds of saving throws:</p>
<p>Fortitude (constitution), reflex (dex), and will (wis).</p>
<h3>Death</h3>
<p>When your hit points hit zero, you&rsquo;re dying. You can&rsquo;t take an actions. Make a DC 10 constitution check to become stable. If you don&rsquo;t succeed, take a point of damage.</p>
<p>If a dying character gets any sort of healing, theyre stable. When their hitpoints get above zero again, they can function.</p>
<h3>Kinds of actions</h3>
<p>Standard- most things are standard actions. Can only be performed on your turn.</p>
<p>Movement- all movement is a movement action.</p>
<p>Bonus- a free action in addition to the standard and movement actions you regularly get on your turn. Some abilities are bonus actions. You can only take one bonus action on your turn.</p>
<p>Immediate- these abilities can be done at any time, even when it&rsquo;s not your turn.</p>
<h3>General DC difficulties</h3>
<p><a></a><a></a></p>
<table>
<tbody>
<tr>
<td colspan="1" rowspan="1">
<p>very easy</p>
</td>
<td colspan="1" rowspan="1">
<p>5</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>easy</p>
</td>
<td colspan="1" rowspan="1">
<p>10</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>medium</p>
</td>
<td colspan="1" rowspan="1">
<p>15</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>hard</p>
</td>
<td colspan="1" rowspan="1">
<p>20</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>very hard</p>
</td>
<td colspan="1" rowspan="1">
<p>25</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>nearly impossible</p>
</td>
<td colspan="1" rowspan="1">
<p>30</p>
</td>
</tr>
</tbody>
</table>
<h3>Ability modifiers</h3>
<p><a></a><a></a></p>
<table>
<tbody>
<tr>
<td colspan="1" rowspan="1">
<p>1</p>
</td>
<td colspan="1" rowspan="1">
<p>-5</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>2,3</p>
</td>
<td colspan="1" rowspan="1">
<p>-4</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>4,5</p>
</td>
<td colspan="1" rowspan="1">
<p>-3</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>6,7</p>
</td>
<td colspan="1" rowspan="1">
<p>-2</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>8,9</p>
</td>
<td colspan="1" rowspan="1">
<p>-1</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>10,11</p>
</td>
<td colspan="1" rowspan="1">
<p>0</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>12,13</p>
</td>
<td colspan="1" rowspan="1">
<p>1</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>14,15</p>
</td>
<td colspan="1" rowspan="1">
<p>2</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>16,17</p>
</td>
<td colspan="1" rowspan="1">
<p>3</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>18,19</p>
</td>
<td colspan="1" rowspan="1">
<p>4</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>20,21</p>
</td>
<td colspan="1" rowspan="1">
<p>5</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>21,22</p>
</td>
<td colspan="1" rowspan="1">
<p>6</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>23,24</p>
</td>
<td colspan="1" rowspan="1">
<p>7</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>25,26</p>
</td>
<td colspan="1" rowspan="1">
<p>8</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>27,28</p>
</td>
<td colspan="1" rowspan="1">
<p>9</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>29,30</p>
</td>
<td colspan="1" rowspan="1">
<p>10</p>
</td>
</tr>
</tbody>
</table>
<h2>Sizes</h2>
<p><a></a><a></a></p>
<table>
<tbody>
<tr>
<td colspan="1" rowspan="1">
<p>Size</p>
</td>
<td colspan="1" rowspan="1">
<p>Attack &amp; AC modifier</p>
</td>
<td colspan="1" rowspan="1">
<p>Hide modifier</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>fine</p>
</td>
<td colspan="1" rowspan="1">
<p>+8</p>
</td>
<td colspan="1" rowspan="1">
<p>+16</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>diminutive</p>
</td>
<td colspan="1" rowspan="1">
<p>+4</p>
</td>
<td colspan="1" rowspan="1">
<p>+12</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>tiny</p>
</td>
<td colspan="1" rowspan="1">
<p>+2</p>
</td>
<td colspan="1" rowspan="1">
<p>+8</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>small</p>
</td>
<td colspan="1" rowspan="1">
<p>+1</p>
</td>
<td colspan="1" rowspan="1">
<p>+4</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>medium</p>
</td>
<td colspan="1" rowspan="1">
<p>0</p>
</td>
<td colspan="1" rowspan="1">
<p>0</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>large</p>
</td>
<td colspan="1" rowspan="1">
<p>-1</p>
</td>
<td colspan="1" rowspan="1">
<p>-4</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>huge</p>
</td>
<td colspan="1" rowspan="1">
<p>-2</p>
</td>
<td colspan="1" rowspan="1">
<p>-8</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>gargantuan</p>
</td>
<td colspan="1" rowspan="1">
<p>-4</p>
</td>
<td colspan="1" rowspan="1">
<p>-12</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>colossal</p>
</td>
<td colspan="1" rowspan="1">
<p>-8</p>
</td>
<td colspan="1" rowspan="1">
<p>-16</p>
</td>
</tr>
</tbody>
</table>
`,
"Combat":`<h2>Combat</h2>
<p>At the start of battle, every participate does a Dex check for initiative. They go in that order for the rest of battle. If one side is surprising the other side, they get a single free round of actions. Every round is 6 seconds.</p>
<p>On your turn, you get a move action and a regular action. The move action can be used to move a distance up to your speed, and the regular action is used for everything else. Also, certain abilities can be used as bonus actions- these are noted in their descriptions. You can only take one bonus action per turn.</p>
<h3>Standard combat actions</h3>
<p>These are actions that everyone can perform.</p>
<h4>Attack</h4>
<p>You make one melee or ranged attack. Roll a d20 and add your attack modifier, if that beats the opponents AC then it&rsquo;s a hit! </p>
<p>The attack modifier for a melee weapon is your base attack bonus + strength modifier + size modifier.</p>
<p>The attack modifier for a ranged weapon is the base attack bonus + dex mod + size mod + range penalty.</p>
<p>If you roll a 1, that&rsquo;s a miss. If you roll a 20, roll again- and if the roll also hits, then you crit. Roll your damage (plus all your bonuses) twice.</p>
<h4>Dash</h4>
<p>Doubles your speed for this turn.</p>
<h4>Disengage</h4>
<p>Your movement doesn&rsquo;t provoke attacks of opportunity.</p>
<h4>Dodge</h4>
<p>Any attack roll made against you until the start of your next turn has disadvantage, and you have the advantage in any dex checks.</p>
<p>for an <a href="https://statmodeling.stat.columbia.edu/2014/07/12/dnd-5e-advantage-disadvantage-probability/">explanation of advantage and disadvantage</a></p>
<h4>Help</h4>
<p>You can give an ally a helping hand with any ability check, giving them advantage if it&rsquo;s performed before the start of your next turn. You can also use this to give an ally advantage on an attack roll on a monster that&rsquo;s within five feet of you.</p>
`,
"Abilities":`<h2>Abilities</h2>
<p>Abilities are, broadly, things that you can do in the game. The unrestricted ones can be taken by any class. Each level, players are awarded a certain number of ability points that they can put into any of their abilities. These points make the abilities more effective and can add different behaviors.</p>
<h4>Appraise (Int)</h4>
<p>A DC 20 check determines the value of an item. If you succeed by 5 or more you will also be told if the item is magical. If you fail, the value will be inaccurate. Rarer or exotic items could increase the DC.</p>
<h4>Bluff (Char)</h4>
<p>Bluff is most commonly used for lying. Other common uses include making actions inconspicuous or combined with diplomacy to coax an npc into doing a particular thing. This check is rolled against the target&rsquo;s passive Sense Motive ability, and various contextual factors like how believable the lie is affects the PC&rsquo;s check.</p>
<h4>Athletics (Str)</h4>
<p>Used for various sportsy things, like climbing or jumping or swimming or throwing things. The DC is determined contextually.</p>
<h4>Diplomacy (Char)</h4>
<p>Used to gather information, to get favors, and to influence npc&rsquo;s. The base DC table for these things is based on the npc&rsquo;s starting orientation towards you:<br /></p>
<p><a></a><a></a></p>
<table>
<tbody>
<tr>
<td colspan="1" rowspan="1">
<p>hostile</p>
</td>
<td colspan="1" rowspan="1">
<p>25+ target&rsquo;s char</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>unfriendly</p>
</td>
<td colspan="1" rowspan="1">
<p>20+ target&rsquo;s char</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>indifferent</p>
</td>
<td colspan="1" rowspan="1">
<p>15+ target&rsquo;s char</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>friendly</p>
</td>
<td colspan="1" rowspan="1">
<p>10+ target&rsquo;s char</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>helpful</p>
</td>
<td colspan="1" rowspan="1">
<p>target&rsquo;s char</p>
</td>
</tr>
</tbody>
</table>
<p>You can improve your relation with an npc by two levels by succeeding the checks, but if you fail one them by 5 or more your level will be lowered.</p>
<p>Requests can be made of people that are at least indifferent to you. The DC modifiers for some sample situations are here.</p>
<p><a></a><a></a></p>
<table>
<tbody>
<tr>
<td colspan="1" rowspan="1">
<p>Give simple advice/directions</p>
</td>
<td colspan="1" rowspan="1">
<p>-5</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>Give detailed advice</p>
</td>
<td colspan="1" rowspan="1">
<p>0</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>Give simple aid</p>
</td>
<td colspan="1" rowspan="1">
<p>0</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>Reveal an unimportant secret</p>
</td>
<td colspan="1" rowspan="1">
<p>+5</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>Reveal an important secret</p>
</td>
<td colspan="1" rowspan="1">
<p>+10</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>Recruit to party</p>
</td>
<td colspan="1" rowspan="1">
<p>+15</p>
</td>
</tr>
</tbody>
</table>
<p>To make an npc do something that they think they came up with, you have to first succeed a Bluff check with a -10 penalty vs the opponent&rsquo;s passive Sense Motive. Then, the creature is treated as friendly for the diplomacy check. The base DC depends on the action.</p>
<h4>Disguise (Char)</h4>
<p>This ability cannot be used if it&rsquo;s untrained.</p>
<p>Your disguise check is opposed by perception checks by suspicious npcs. The initial disguise check is rolled in secret, so the PC does not know how good their disguise is. Npcs get bonuses on their perception checks based on how well they know the person that the player is disguised as.</p>
<p><a></a><a></a></p>
<table>
<tbody>
<tr>
<td colspan="1" rowspan="1">
<p>acquainted</p>
</td>
<td colspan="1" rowspan="1">
<p>+4</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>friends</p>
</td>
<td colspan="1" rowspan="1">
<p>+6</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>good friends</p>
</td>
<td colspan="1" rowspan="1">
<p>+8</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>intimate</p>
</td>
<td colspan="1" rowspan="1">
<p>+10</p>
</td>
</tr>
</tbody>
</table>
<h4>Heal (Wis)</h4>
<p>You can use first aid to heal a dying character. If a character has negative hit points and is losing them, you can make a check against 15 DC to stabilise them. Stabilised characters don&rsquo;t regain health, but they stop losing it.</p>
<h4>Intimidate (Char)</h4>
<p>You can use the intimidate skill to demoralise opponents in combat. The DC of this is 10 + the target&rsquo;s hit dice + the target&rsquo;s Wis modifier. If you succeed, the target is shaken for 1 full round (they take a -2 penalty on all rolls). For every 5 points you beat the DC by, the target is shaken for another round.</p>
<p>You can also use it to persuade people out of combat. The DC is the same, but a failure will raise the DC by 5 points. If it succeeds, the npc will be cooperative for some time, after which they might report you to local authorities. If you are larger than your opponent, take a +4 bonus. If you&rsquo;re smaller than the opponent, take a -4 penalty.</p>
<h4>Knowledge (Int)</h4>
<p>All knowledge categories cannot be used without training.</p>
<p>Ideas for categories:</p>
<p>Geography</p>
<p>Divinity (this covers both the gods themselves and the worshippers)</p>
<p>Local</p>
<p>Magick</p>
<p>Nature</p>
<p>Infernal</p>
<p>Tech </p>
<h4>Linguistics (Int)</h4>
<p>For each point you put into linguistics, you gain another slot for a learned language. To learn a language you must interact with a person speaking that language.</p>
<p>You can also decipher texts written in a language you do not know. The base DC is 20 for simple texts, 25 for regular ones, and 30 for complex ones. This does not count for Old High Gvsqi.</p>
<h4>Perception (Wis)</h4>
<p>Perception has a bunch of uses, but the most common is to be rolled against an opponent's stealth check to see if you detect them. A PC can manually choose to &ldquo;search&rdquo; the environment if they suspect someone might be hiding, otherwise when in proximity a stealthed enemy&rsquo;s roll is judged against the PC&rsquo;s passive perception, which is 10 + wisdom modifier.</p>
<h4>Perform (Char)</h4>
<p>This is divided into nine categories, each with their own ranks:</p>
<p>Act (comedy, drama, pantomime)</p>
<p>Comedy (buffoonery, limericks, joke-telling)</p>
<p>Dance (ballet, waltz, jig)</p>
<p>Keyboard instruments (harpsichord, piano, pipe organ)</p>
<p>Oratory (epic, ode, storytelling)</p>
<p>Percussion instruments (bells, chimes, drums, gong)</p>
<p>String instruments (fiddle, harp, lute, mandolin)</p>
<p>Wind instruments (flute, pan pipes, recorder, trumpet)</p>
<p>Sing (ballad, chant, melody)</p>
<p>A masterwork instrument gives a +2 modifier on perform checks that use it.</p>
<p>There isn&rsquo;t really a DC, the roll just determines the quality of the performance</p>
<p><a></a><a></a></p>
<table>
<tbody>
<tr>
<td colspan="1" rowspan="1">
<p>10</p>
</td>
<td colspan="1" rowspan="1">
<p>meh</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>15</p>
</td>
<td colspan="1" rowspan="1">
<p>nice</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>20</p>
</td>
<td colspan="1" rowspan="1">
<p>great!</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>25</p>
</td>
<td colspan="1" rowspan="1">
<p>whoa!!</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>30</p>
</td>
<td colspan="1" rowspan="1">
<p>WOW</p>
</td>
</tr>
<tr>
<td colspan="1" rowspan="1">
<p>35</p>
</td>
<td colspan="1" rowspan="1">
<p>INCREDIBLE</p>
</td>
</tr>
</tbody>
</table>
<h4>Sense Motive (Wis)</h4>
<p>You can use sense motive if you feel like there&rsquo;s something fishy about what a person is telling you. The normal DC for this use is 20.</p>
<p>You can also use this to determine if someone is enchanted- the DC for this is 25.</p>
<h4>Sleight of Hand (Dex)</h4>
<p>Cannot be used without training.</p>
<p>Sleight of hand can be used to hide small objects like a hand crossbow or dagger on your body. Your sleight of hand check is weighed against the searcher&rsquo;s perception check plus 4 bonus, since they&rsquo;re specifically trying to find objects on you. If the object is a dagger, you take a +2 bonus. If the object is smaller than the palm of your hand, take a +4 bonus.</p>
<p>Sleight of hand can also be used to pickpocket small objects from people. The DC for this is 20.</p>
<h4>Old High Gvsqi (Wis?)</h4>
<p>Cannot be used without training.</p>
<p>Not sure about this one. Read more about the spellcasting systems in 5e and pathfinder.</p>
<h4>Stealth (Dex)</h4>
<p>Your stealth check is weighed against the passive perception score of enemies that aren&rsquo;t aware of your presence. You can move up to half your normal speed, and can stay stealthed as long as you end your turn out of view.</p>
<p>You can&rsquo;t hide when people are aware of you, but a successful bluff check gives you the distraction you need to make the check.</p>
<p>If you&rsquo;re stealthed, you can make a ranged attack and then make a stealth check as a move action. You take a -20 penalty on this check.</p>
`
}
