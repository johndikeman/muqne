var assert = require("assert");
var _ = require("underscore");
var character_creation_data = require("./../public/utils/char_creation.js");

var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

describe("character selection", function(){
    const driver = new webdriver.Builder().forBrowser('chrome').build();
    it("checks that the values of things are correct when clicking on the stuff",async function(){
        try{
            await driver.get("http://localhost:8000/php/character_creator.php");
            var racebar = await driver.findElement(By.id('race'));
            await racebar.click();
            await driver.sleep(200);
            await racebar.findElement(By.css('option[value="elf"]')).click();
            var culturebar = await driver.findElement(By.id('culture-group'));
            await culturebar.click();
            await driver.sleep(200);
            var options = await culturebar.findElements(By.css("option"));
            var needed_element = [];
            for(let element of options){
                text = await element.getText()
                if(text == "elf_regular"){
                    await needed_element.push(element);
                }
            }
            //await console.log(needed_element);
            await assert(needed_element.length > 0);
            await needed_element[0].click();

            var classbar = await driver.findElement(By.id('class'));
            await classbar.click();
            await driver.sleep(200);
            var options = await classbar.findElements(By.css("option"));
            var needed_element = await []
            for(let element of options){
                text = await element.getText();
                if(text == "barbarian"){
                    await needed_element.push(element);
                }
            }
            await assert(needed_element.length > 0);
            await needed_element[0].click();

            var abilities = await driver.findElement(By.id("ability-list"));
            var ability_elements = await abilities.findElements(By.css("li"));
            await ability_elements[0].click();
            var ability_element_class = await ability_elements[0].getAttribute("class");
            await driver.sleep(200);
            await console.log(ability_element_class);
            await assert(ability_element_class.includes("active"));
            await ability_elements[1].click();
            var ability_element_class = await ability_elements[1].getAttribute("class");
            await assert(ability_element_class.includes("active"));
            var ability_element_class = await ability_elements[0].getAttribute("class");
            await assert(!ability_element_class.includes("active"));

        }finally{
            driver.close();
            driver.quit();
        }
    });
    it.only("confirms persistance of built characters",async function(){
        try{
            await driver.get("http://localhost:8000/php/character_creator.php");
            var racebar = await driver.findElement(By.id('race'));
            await racebar.click();
            await driver.sleep(200);
            await racebar.findElement(By.css('option[value="elf"]')).click();
            var culturebar = await driver.findElement(By.id('culture-group'));
            await culturebar.click();
            await driver.sleep(200);
            var options = await culturebar.findElements(By.css("option"));
            var needed_element = [];
            for(let element of options){
                text = await element.getText()
                if(text == "elf_regular"){
                    await needed_element.push(element);
                }
            }
            //await console.log(needed_element);
            await assert(needed_element.length > 0);
            await needed_element[0].click();

            var classbar = await driver.findElement(By.id('class'));
            await classbar.click();
            await driver.sleep(200);
            var options = await classbar.findElements(By.css("option"));
            var needed_element = await []
            for(let element of options){
                text = await element.getText();
                if(text == "barbarian"){
                    await needed_element.push(element);
                }
            }
            await assert(needed_element.length > 0);
            await needed_element[0].click();
            // at this point we have a barbarian in the window character object
            await driver.findElement(By.id("char-firstname")).sendKeys("wilson");
            var submit = await driver.findElement(By.id("submit-button"))
            await submit.click();
            await driver.sleep(1000);

            var element = await driver.findElement(By.className("character-list-element"))
            await driver.sleep(2000);
            res = await element.getText();
            assert.equal(res,"wilson");

            element.click();
            await driver.sleep(20000);

        }finally{
            driver.close();
            driver.quit();
        }
    });
})
