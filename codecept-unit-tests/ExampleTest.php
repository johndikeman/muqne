<?php 
include __DIR__ . "/../env.php";
include __DIR__ . "/../public/php/accounts.php";
include __DIR__ . "/../public/php/utils.php";
include __DIR__ . "/../public/php/character_object_store.php";

class ExampleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testLogin()
    {
        $username = "charlie";
        $password = "brown";

        create_account($username,$password);
        $this->tester->seeInDatabase("Users",['username' => $username,'password'=>$password]);

    }

    public function testCharacterCreation(){
        $this->tester->haveInDatabase("Users",["id"=>50,"username"=>"twix","password"=>"idk"]);
        
        $json_string = '{"firstname":"wef","lastname":"wef","race":"dwarf","culture_group":"elven_dedicant","class":"warrior","abilities":[{"name":"cleave","desc":"Cleave can be used to target multiple enemies with an attack. After the attack, you\u0019re staggered for an amount of turns equal to the number of enemies you hi","level":0},{"name":"parry","desc":"You can choose to parry an oncoming attack, if you win a dex check with the DC being the attack roll of the opponent. If you get it, you take no damage and the opponent is staggered for 3 turns. If you miss, you leave yourself exposed & the enemy gets advantage on their damage roll.","level":0},{"name":"agressive stance","desc":"Activating this ability gives you a permanent +2 on attack rolls but -1 on AC. each point in this ability increases the attack roll bonus by 1 and increases the AC penalty by ½.","level":0},{"name":"defensive stance","desc":"Activating this ability gives you a permanent +2 on AC but -1 on attack rolls. each point in this ability increases the AC bonus by 1 and increases the attack roll penalty by ½.","level":0}],"attributes":[{"name":"strength","value":0,"max":-1},{"name":"hitpoints","value":10,"max":10},{"name":"dexterity","value":0,"max":-1},{"name":"constitution","value":0,"max":-1},{"name":"intelligence","value":0,"max":-1},{"name":"wisdom","value":0,"max":-1},{"name":"charisma","value":0,"max":-1}],"bio":"wef"}';

        add_character_to_db($json_string,50);
        $this->tester->seeInDatabase("Characters",['firstname' => 'wef','lastname'=>"wef"]);
        $abilities = $this->tester->grabFromDatabase("Characters","abilities",["owner"=>1,'firstname' => 'wef','lastname'=>"wef"]);
        $attributes = $this->tester->grabFromDatabase("Characters","attributes",["owner"=>1,'firstname' => 'wef','lastname'=>"wef"]);
        codecept_debug($abilities);
        codecept_debug($attributes);


    }

    public function testCharacterReception(){
        $this->tester->haveInDatabase("Users",["id"=>1,"username"=>"twix","password"=>"idk"]);
        $this->tester->haveInDatabase("Users",["id"=>2,"username"=>"wowee","password"=>"idk"]);
        $this->tester->haveInDatabase("Characters",["owner"=>1,"firstname"=>"fella","lastname"=>"gee"]);
        $this->tester->haveInDatabase("Characters",["owner"=>1,"firstname"=>"heckin","lastname"=>"doge"]);
        $this->tester->haveInDatabase("Characters",["owner"=>2,"firstname"=>"frick","lastname"=>"frack"]);

        codecept_debug(get_characters_from_db(1));

    }
}
