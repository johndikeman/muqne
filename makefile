sass:
	./node_modules/sass/sass.js ./public/src/sass:./public/bin/css

js:
	./node_modules/browserify/bin/cmd.js ./public/src/js/main.js -o ./public/bin/js/main.js
	./node_modules/browserify/bin/cmd.js ./public/src/js/character_creator.js -o ./public/bin/js/character_creator.js
	./node_modules/browserify/bin/cmd.js ./public/src/js/login.js -o ./public/bin/js/login.js
	./node_modules/browserify/bin/cmd.js ./public/src/js/lore.js -o ./public/bin/js/lore.js
test-front: js
	./node_modules/.bin/mocha ./mocha_tests --no-timeout

test-back:
	 docker-compose exec php-fpm php vendor/bin/codecept run unit --debug
